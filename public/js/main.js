$("body").append(`
    <div id="loader-wrapper" style="
        width: 100vw;
        height: 100vh;
        background: white;
        position: absolute;
        top: 0;
        right: 0;
        display: flex;
        align-items: center;
        justify-content: center;
    ">
        <img src="/img/Infinity.svg" alt="loader">
    </div>
`);

document.onreadystatechange = function()
{
    if (document.readyState != "complete")
    {
      document.querySelector("body").style.visibility = "hidden";
      document.querySelector("#loader-wrapper").style.visibility = "visible";
    }

    else
    {
      document.querySelector("#loader-wrapper").style.display = "none";
      document.querySelector("body").style.visibility = "visible";
    }
};


// -------------------------------------------------------
// MENU

$("#submit" ).click(function(e) {
    e.preventDefault();
    $(".modal-send").show();
});

function fermerModal (e) {
    $(".modal-send").hide();
}

const previewImport = (event) => {
    $('#previewItem').html('');

    if (event.target.files && event.target.files.length > 0) {
        for (let file of event.target.files) {
            $('#previewItem').append(`
            <div style="
                width: 100%;
                height: auto;
                font-size: 0.8rem;
                font-weight: 600;
                background: rgba(0, 0, 0, 0.152);
                display: flex;
                padding: 0.3rem;
            ">
                <div>
                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" class="bi bi-file-earmark-plus-fill" viewBox="0 0 16 16">
                    <path d="M9.293 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.707A1 1 0 0 0 13.707 4L10 .293A1 1 0 0 0 9.293 0zM9.5 3.5v-2l3 3h-2a1 1 0 0 1-1-1zM8.5 7v1.5H10a.5.5 0 0 1 0 1H8.5V11a.5.5 0 0 1-1 0V9.5H6a.5.5 0 0 1 0-1h1.5V7a.5.5 0 0 1 1 0z"/>
                </svg>
                ${file.name}
                </div>
            </div>`);
        }
    }
}

$("#import").on('change', previewImport);

function btnColorOff() {
    $(".column-one .bouton-one").css({"background-color": "white"});
    $(".column-one .bouton-two").css({"background-color": "white"});
    $(".column-one .bouton-three").css({"background-color": "white"});
    $(".column-one .bouton-for").css({"background-color": "white"});
    $(".column-one .bouton-five").css({"background-color": "white"});
    $(".column-one .bouton-six").css({"background-color": "white"});
    $(".column-one .bouton-seven").css({"background-color": "white"});
    $(".column-one .bouton-height").css({"background-color": "white"});
}

function btnColumnTwoOff() {
    $(".column-three").hide();
    $(".column-two").hide();
}

$(".column-one .bouton-one").click(function () {
    btnColorOff();
    $(this).css({"background-color": "rgba(0, 0, 0, 0.5)"});

    btnColumnTwoOff();
    $(".column-two").show();

    $(".column-two #form-1").hide();
    $(".column-two #form-2").hide();

    $(".column-two #form-1").show();
    $(".title-docs").html("Attestation de travail");
});

$(".column-one .bouton-two").click(function () {
    btnColorOff();
    $(this).css({"background-color": "rgba(0, 0, 0, 0.5)"});

    btnColumnTwoOff();
    $(".column-two").show();
    $(".column-two #form-1").hide();
    $(".column-two #form-2").hide();

    $(".column-two #form-2").show();
    $(".title-docs").html("Attestation de présence");
});

$(".column-one .bouton-three").click(function () {
    btnColorOff();
    $(this).css({"background-color": "rgba(0, 0, 0, 0.5)"});

    btnColumnTwoOff();
    $(".column-two").show();
    $(".title-docs").html("Attestation d'absence");
});

$(".column-one .bouton-for").click(function () {
    btnColorOff();
    $(this).css({"background-color": "rgba(0, 0, 0, 0.5)"});

    btnColumnTwoOff();
    $(".column-two").show();
    $(".title-docs").html("Attestation de revenu");
});

$(".column-one .bouton-five").click(function () {
    btnColorOff();
    $(this).css({"background-color": "rgba(0, 0, 0, 0.5)"});

    btnColumnTwoOff();
    $(".column-two").show();
    $(".title-docs").html("Attestation d'avancement indiciaire");
    $(".import-docs").hide();
});

$(".column-one .bouton-six").click(function () {
    btnColorOff();
    $(this).css({"background-color": "rgba(0, 0, 0, 0.5)"});

    btnColumnTwoOff();
    $(".column-two").show();
    $(".title-docs").html("Attestation de sortir du territoire");
});

$(".column-one .bouton-seven").click(function () {
    btnColorOff();
    $(this).css({"background-color": "rgba(0, 0, 0, 0.5)"});

    btnColumnTwoOff();
    $(".column-two").show();
    $(".title-docs").html("Autorisation spéciale pour absence");
});

$(".column-one .bouton-height").click(function () {
    btnColorOff();
    $(this).css({"background-color": "rgba(0, 0, 0, 0.5)"});

    btnColumnTwoOff();
    $(".column-two").show();
    $(".title-docs").html("Congés");
});

$("#faire-demande").click(function (){
    $(".column-three").show();
});

// -------------------------------------------------------
// LISTE AGENTS

setTimeout(() => {
    $("#agent-success").hide("fast");
}, 10000);

$("#search-btn" ).click(function(e) {
    // e.preventDefault();
});

setTimeout(() => {
    $("#agent-update-danger").hide("fast");
}, 10000);

// -------------------------------------------------------
// AGENTS

const callPreloadData = (val) => {
    try {
        $.ajax({
            url: 'preload-data-form',
            type: 'GET',
            data: `id=${val}`,
            dataType: 'html',
            beforeSend: function () {
                document.querySelector("body").style.visibility = "hidden";
                document.querySelector("#loader-wrapper").style.visibility = "visible";
            },
            success: function(data) {
                data = data.trim();
                if (data) {
                    // Data
                    $('#form-data-preload').html(data);
                }
                document.querySelector("#loader-wrapper").style.visibility = "hidden";
                document.querySelector("body").style.visibility = "visible";
            },
            error: function(e){
                console.log(e);
                document.querySelector("#loader-wrapper").style.visibility = "hidden";
                document.querySelector("body").style.visibility = "visible";
            }
        })
    } catch (err) {
        console.error(err);
    }
}

$('#modalUpdate').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var id = button.data('whatever');

    callPreloadData(id);
});
