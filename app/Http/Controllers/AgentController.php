<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\Fonction;
use App\Models\Agent;
use Illuminate\Http\Request;

session_start();

function valid_donnees($donnees){
    $donnees = trim($donnees);
    $donnees = stripslashes($donnees);
    $donnees = htmlspecialchars($donnees);

    return $donnees;
}

function firstLetterUpper($mot) {
	$firstLetter = strtoupper(substr($mot, 0, 1));
	$restLetters = strtolower(substr($mot, 1));

	return $firstLetter.''.$restLetters;
}

function breakPhrase($phrase) {
    $remplacer = array(" ");
	
	$recupPhrase = $phrase;
	
	$breakStrPhrase = trim(str_replace($remplacer, " ", $recupPhrase));

	$separateur = "#[ ]+#";
    $mots = preg_split($separateur, $breakStrPhrase);

    $nbrMots = count($mots);

    $return = "";

    for ($i=0; $i < $nbrMots; $i++) { 
		$word = firstLetterUpper($mots[$i]);
        $return = $return.' '.$word;
    }

    return $return;
}

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userlogin = strtolower(valid_donnees($_POST['username']));
        $mdplogin = valid_donnees($_POST['mdp']);

        if(!empty($userlogin)){
            $personSearchLogin = Agent::where('username', $userlogin)->get();
            if(count($personSearchLogin) >= 1){
                foreach ($personSearchLogin as $key => $value) {
                    if($userlogin === $value->username){
                        if($mdplogin === $value->mdp){
                            // Connecter l'utilisateur
                            $_SESSION["id"] = $value->id;
                            $_SESSION["admins"] = $value->administrateur;
                            return redirect('/');
                        }else{
                            // Le mot de passe ne correspont pas
                            return redirect()->back()->withErrors('4');
                        }
                    }else{
                        // Aucun nom d'utilisateur trouver
                        return redirect()->back()->withErrors('3');
                    }
                }
            }else{
                return redirect()->back()->withErrors('2');
            }
        }else{
            return redirect()->back()->withErrors('1');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (isset($_SESSION["id"]) && !empty($_SESSION["id"])) {
            // Connected
            return view('ajouter-agent');
        } else {
            return redirect('/connexion');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($_SESSION["id"]) && !empty($_SESSION["id"])){

            $_SESSION['input_nom'] = trim(breakPhrase(valid_donnees($request['nom'])));
            $_SESSION['input_prenom'] = trim(breakPhrase(valid_donnees($request['prenom'])));
            $_SESSION['input_tel'] = valid_donnees($request['tel']);
            $_SESSION['input_sexe'] = intval(valid_donnees($request['sexe']));
            $_SESSION['input_date_naissance'] = valid_donnees($request['date_naissance']);
            $_SESSION['input_lieu_naissance'] = trim(breakPhrase(valid_donnees($request['lieu_naissance'])));
            $_SESSION['input_id_fonction'] = intval(valid_donnees($request['id_fonction']));
            $_SESSION['input_id_service'] = intval(valid_donnees($request['id_service']));
            $_SESSION['input_adresse'] = trim(breakPhrase(valid_donnees($request['adresse'])));
            $_SESSION['input_email'] = valid_donnees(strtolower($request['email']));
            $_SESSION['input_username'] = valid_donnees(strtolower($request['username']));
            $_SESSION['input_admins'] = intval(valid_donnees($request['admins']));

            $request->validate([
                'nom' => ['required', 'between:1,30'],
                'prenom' => ['required', 'between:1,30'],
                'tel' => ['required','numeric'],
                'sexe' => ['required', 'between:1,2'],
                'date_naissance' => ['required', 'before:01-01-2010'],
                'lieu_naissance' => ['required', 'between:1,125'],
                'id_service' => ['required', 'numeric', 'min:1'],
                'id_fonction' => ['required','numeric', 'min:1'],
                'adresse' => ['nullable', 'max:255'],
                'email' => ['required', 'email'],
                'username' => ['required', 'unique:agents'],
                'mdp' => ['required', 'min:8', 'same:mdp2'],
                'admins' => ['required', 'boolean']
            ]);

            Agent::create([
                'nom' => trim(breakPhrase(valid_donnees($request['nom']))),
                'prenom' => trim(breakPhrase(valid_donnees($request['prenom']))),
                'tel' => valid_donnees($request['tel']),
                'sexe' => intval(valid_donnees($request['sexe'])),
                'date_naissance' => valid_donnees($request['date_naissance']),
                'lieu_naissance' => trim(breakPhrase(valid_donnees($request['lieu_naissance']))),
                'id_fonction' => intval(valid_donnees($request['id_fonction'])),
                'id_service' => intval(valid_donnees($request['id_service'])),
                'adresse' => trim(breakPhrase(valid_donnees($request['adresse']))),
                'email' => valid_donnees(strtolower($request['email'])),
                'username' => valid_donnees(strtolower($request['username'])),
                'mdp' => $request->mdp,
                'admins' => intval(valid_donnees($request['admins'])),
            ]);

            unset($_SESSION['input_nom']);
            unset($_SESSION['input_prenom']);
            unset($_SESSION['input_tel']);
            unset($_SESSION['input_sexe']);
            unset($_SESSION['input_date_naissance']);
            unset($_SESSION['input_lieu_naissance']);
            unset($_SESSION['input_id_fonction']);
            unset($_SESSION['input_id_service']);
            unset($_SESSION['input_adresse']);
            unset($_SESSION['input_email']);
            unset($_SESSION['input_username']);
            unset($_SESSION['input_admins']);

            $message = "L'employé ".trim(breakPhrase(valid_donnees($request['nom'])))." ".trim(breakPhrase(valid_donnees($request['prenom'])))." a été ajouter avec success";
            return redirect('/liste-agents')->with('success', $message);
        }else {
            return redirect('/connexion');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id = null)
    {
        if (isset($_SESSION["id"]) && !empty($_SESSION["id"])) {
            // Connected
            $id = $_SESSION['id'];

            if (intval($request['form']) === 1) {
                $update_tel = valid_donnees($request['tel']);

                $update_adresse = valid_donnees($request['adresse']);
                $update_adresse = trim(breakPhrase($update_adresse));

                $update_email = valid_donnees($request['email']);

                if (!empty($update_tel) && strlen($update_tel) === 10) {
                    if (!empty($update_adresse)) {
                        if (!empty($update_email)) {
                            $update_info = Agent::find($id)->update([
                                'tel' => $update_tel,
                                'adresse' => $update_adresse,
                                'email' => $update_email,
                            ]);
    
                            $update_phrase_success = "Les modifications apporté ont été enregistré avec success";
                            return redirect()->back()->with('success', $update_phrase_success);
                        }else {
                            //Error 5
                            return redirect()->back()->withErrors('5');
                        }
                    }else {
                        //Error 4
                        return redirect()->back()->withErrors('4');
                    }
                }else {
                    //Error 3
                    return redirect()->back()->withErrors('3');
                }
            }else if(intval($request['form']) === 2) {
                $old_mdp = valid_donnees($_POST['old_mdp']);
                $update_mdp1 = valid_donnees($_POST['mdp']);
                $update_mdp2 = valid_donnees($_POST['mdp2']);

                if($update_mdp1 === $update_mdp2){
                    $update_mdp = $update_mdp1;
                }else {
                    $update_mdp = null;
                }

                $mdp_search_update = Agent::find($id);
                $mdp_search_update = $mdp_search_update->mdp;

                if ($old_mdp === $mdp_search_update) {
                    if ($update_mdp !== null) {
                        $update_connexion = Agent::find($id)->update([
                            'mdp' => $update_mdp,
                        ]);

                        $update_phrase_success = "Les modifications apporté ont été enregistré avec success";
                        return redirect()->back()->with('success', $update_phrase_success);
                    }else {
                        //Error 2
                        return redirect()->back()->withErrors('2');
                    }
                }else {
                    //Error 1
                    return redirect()->back()->withErrors('1');
                }
            }else if(intval($request['form']) === 3){
                $id = valid_donnees($request['id']);

                $update_id_fonction = "null";
                $update_id_service = "null";

                $err = array();

                if (isset($request['nom'])){
                    $update_nom = valid_donnees($request['nom']);
                    $update_nom = trim(breakPhrase($update_nom));

                    if(!empty($update_nom)){
                        $update_info_nom = Agent::find($id)->update([
                            'nom' => $update_nom,
                        ]);
                    }else{
                        array_push($err, "1");
                    }
                }

                if (isset($request['prenom'])){
                    $update_prenom = valid_donnees($request['prenom']);
                    $update_prenom = trim(breakPhrase($update_prenom));

                    if(!empty($update_prenom)){
                        $update_info_prenom = Agent::find($id)->update([
                            'prenom' => $update_prenom,
                        ]);
                    }else{
                        array_push($err, "2");
                    }
                }

                if (isset($request['tel'])){
                    $update_tel = valid_donnees($request['tel']);

                    if (!empty($update_tel) && strlen($update_tel) === 10){
                        $update_info_tel = Agent::find($id)->update([
                            'tel' => $update_tel,
                        ]);
                    }else{
                        array_push($err, "3");
                    }
                }

                if (isset($request['adresse'])){
                    $update_adresse = valid_donnees($request['adresse']);
                    $update_adresse = trim(breakPhrase($update_adresse));

                    if (!empty($update_adresse) && strlen($update_adresse) === 10){
                        $update_info_adresse = Agent::find($id)->update([
                            'adresse' => $update_adresse,
                        ]);
                    }
                }

                if (isset($request['fonction'])){
                    $update_select_fonction = intval(valid_donnees($request['fonction']));

                    $table_fonction = Fonction::all()->sortBy('id');
                    $count_table_fonction = count($table_fonction);
                    for ($i=0; $i<=$count_table_fonction; $i++){
                        if($update_select_fonction === $i){
                            $update_id_fonction = $i;
                        }
                    }

                    $update_fonction = $update_id_fonction;

                    if (!empty($update_fonction)) {
                        $update_info_fonction = Agent::find($id)->update([
                            'id_fonction' => $update_fonction,
                        ]);
                    }
                }

                if (isset($request['service'])){
                    $update_select_service = intval(valid_donnees($request['service']));

                    $table_service = Service::all()->sortBy('id');
                    $count_table_service = count($table_service);
                    for ($i=0; $i<=$count_table_service; $i++){
                        if($update_select_service === $i){
                            $update_id_service = $i;
                        }
                    }

                    $update_service = $update_id_service;
                    
                    if (!empty($update_service) && $update_service !== "null"){
                        $update_info_service = Agent::find($id)->update([
                            'id_service' => $update_service,
                        ]);
                    }else{
                        array_push($err, "5");
                    }
                }

                if (isset($request['email'])) {
                    $update_email = valid_donnees($request['email']);

                    if (!empty($update_email)){
                        $update_info_email = Agent::find($id)->update([
                            'email' => $update_email,
                        ]);
                    }else{
                        array_push($err, "6");
                    }
                }

                if (isset($request['username']) && $request['username'] !== null) {
                    $update_username = valid_donnees($request['username']);

                    if (!empty($update_username)) {
                        $searchUsername = Agent::where('username', $update_username)->get();
                        if(count($searchUsername) <= 0){
                            $update_info_username = Agent::find($id)->update([
                                'username' => $update_username,
                            ]);
                        }else{
                            array_push($err, "8");
                        }
                    }else{
                        array_push($err, "7");
                    }
                }

                if (isset($request['mdp']) && isset($request['mdp2']) && $request['mdp'] !== null && $request['mdp2'] !== null) {
                    $update_mdp1 = valid_donnees($request['mdp']);
                    $update_mdp2 = valid_donnees($request['mdp2']);

                    if($update_mdp1 === $update_mdp2){
                        $update_mdp = $update_mdp1;
                    }else {
                        $update_mdp = '';
                    }

                    if (!empty($mdp1) && !empty($mdp2)){
                        if ($mdp1 === $mdp2){
                            $update_info_mdp = Agent::find($id)->update([
                                'mdp' => $update_mdp,
                            ]);
                        }else{
                            array_push($err, "10");
                        }
                    }else{
                        array_push($err, "9");
                    }
                }

                if (isset($request['admin'])){
                    $update_admin = valid_donnees($request['admin']);
                    $update_admin = intval($update_admin);

                    if (empty($update_admin)) {
                        $update_admin = 0;
                    }else {
                        if ($update_admin === 1) {
                            $update_admin = 1;
                        }else {
                            $update_admin = 0;
                        }
                    }

                    if(!empty($update_admin)){
                        $update_info_admin = Agent::find($id)->update([
                            'admins' => $update_admin,
                        ]);
                    }else{
                        array_push($err, "11");
                    }
                }

                // dd($err);

                // return redirect()->back()->withErrors('listError', $err);
                $update_phrase_success = "Les modifications valident ont été enregistré avec success";
                return redirect()->back()->with('return', $update_phrase_success);

            }else{
                return redirect()->back();
            }
        } else {
            return redirect('/connexion');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
