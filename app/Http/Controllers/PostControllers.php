<?php

namespace App\Http\Controllers;

use App\Models\Agent;

use Illuminate\Http\Request;

session_start();

class PostControllers extends Controller
{
    public function index() {
        if (isset($_SESSION["id"]) && !empty($_SESSION["id"])) {
            // Connected
            return view('home');
        } else {
            return redirect('/connexion');
        }
    }

    public function connexion() {
        return view('connexion');
    }

    public function logout() {
        if (isset($_SESSION["id"]) && !empty($_SESSION["id"])) {
            // Connected
            unset($_SESSION["id"]);
            unset($_SESSION['admins']);
            return redirect('/connexion');
        } else {
            return redirect('/connexion');
        }
    }

    public function messageries() {
        if (isset($_SESSION["id"]) && !empty($_SESSION["id"])) {
            // Connected
            return view('messageries');
        } else {
            return redirect('/connexion');
        }
    }

    public function agents() {
        if (isset($_SESSION["id"]) && !empty($_SESSION["id"])) {
            // Connected
            return view('agents');
        } else {
            return redirect('/connexion');
        }
    }

    public function pointages() {
        if (isset($_SESSION["id"]) && !empty($_SESSION["id"])) {
            // Connected
            return view('pointages');
        } else {
            return redirect('/connexion');
        }
    }

    public function menu() {
        if (isset($_SESSION["id"]) && !empty($_SESSION["id"])) {
            // Connected
            return view('profil');
        } else {
            return redirect('/connexion');
        }
    }

    public function profil() {
        if (isset($_SESSION["id"]) && !empty($_SESSION["id"])) {
            // Connected
            return view('profil');
        } else {
            return redirect('/connexion');
        }
    }

    public function modifierProfil() {
        if (isset($_SESSION["id"]) && !empty($_SESSION["id"])) {
            // Connected
            return view('modifier-profil');
        } else {
            return redirect('/connexion');
        }
    }

    public function documentsAdministratifs() {
        if (isset($_SESSION["id"]) && !empty($_SESSION["id"])) {
            // Connected
            return view('documents-administratifs');
        } else {
            return redirect('/connexion');
        }
    }

    public function preloadDataForm() {
        if (isset($_SESSION["id"]) && !empty($_SESSION["id"])) {
            // Connected
            return view('preloads.data-preload');
        } else {
            return redirect('/connexion');
        }
    }

}
