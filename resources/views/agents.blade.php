<?php 
    use App\Models\Agent;
    use App\Models\Service;
    use App\Models\Fonction;
    use App\Models\Direction;
    use App\Models\Sous_direction;

    $agents = Agent::all()->sortBy('id');
    $countAgents = count($agents);

    $service = Service::all()->sortBy('id');
    $fonction = Fonction::all()->sortBy('id');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Liste des agents</title>
    <!-- Fonts -->
    <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.2.2/font/bootstrap-icons.css" rel="stylesheet">
    
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>

    <!-- Custom asset -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script defer src="{{asset('js/main.js')}}"></script>
</head>
<body class="antialiased">
    <section>

        <div style="
            display: flex;
        ">
            {{-- SLIDER --}}
                <x-comp-slider />
            {{-- FIN SLIDER --}}

            <div style="
                width: 95vw;
                height: 100vh;
                background: rgb(131, 84, 84);
                background-image: url('{{asset('img/fond.jpg')}}');
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-size: cover;
            ">
                {{-- HEADER--}}
                <x-comp-header />
                {{-- FIN HEADER --}}

                <div style="width: 100%; height: 92vh; display: flex; align-items: center; jsutify-content: center;">
                    <div style="width: 100%; height: 92vh; display: flex;">
                        <div style="
                            width: 95%;
                            height: 100%;
                            background: rgba(255, 255, 255, 0.4);
                        ">
                            <div style="
                                height: 100%;
                                padding-left: 2%;
                                padding-top: 2%;
                            ">
                                <div style="
                                    width: 100%;
                                    height: 8%;
                                    display: flex;
                                    align-items: center;
                                    background: white;
                                    border-bottom: solid 2px #EC9628;
                                ">
                                    <div style="
                                        width: 100%;
                                        height: 100%;
                                        display: flex;
                                        flex-direction: column;
                                        justify-content: center;
                                        font-weight: 600;
                                    ">
                                        <form method="GET" class="form-row" style="display: flex;justify-content: space-between;">
                                            <div style="display: flex;padding-left: 2rem;">
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" id="nom_search" name="nom_search" placeholder="Nom">
                                                </div>
                                                <div class="col-md-6" style="margin-left: 1rem;">
                                                    <input type="text" class="form-control" id="prenom_search" name="prenom_search" placeholder="Prenom">
                                                </div>
                                                <div class="col-md-6" style="margin-left: 1rem;">
                                                    <div class="form-group">
                                                        <select class="form-select" id="admin_search" name="admin_search" aria-label="Default select">
                                                            <option selected value="2">Administrateur</option>
                                                            <option value="1">Oui</option>
                                                            <option value="0">Non</option>
                                                          </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div style="display: flex;justify-content: center;padding-right: 2rem;">
                                                <button type="submit" id="search-btn" class="btn btn-primary">
                                                    Rechercher
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div style="height: 2%;"></div>
                                <div style="
                                    width: 100%;
                                    height: 90%;
                                ">
                                    <div style="
                                        width: 100%;
                                        height: 100%;
                                        border-top: solid 1px #EC9628;
                                    ">
                                        <div style="
                                            width: 100%;
                                            height: 10%;
                                            background: white;
                                            padding-left: 1rem;
                                            font-size: 1.1rem;
                                            font-weight: bold;
                                            display: flex;
                                            align-items: center;
                                            border-bottom: solid 3px gray;
                                        ">
                                            Total(s)&nbsp;<span style="color: #EC9628;"> {{$countAgents}}</span>
                                            @if (\Session::has('success'))
                                                <div class="alert alert-success" id="agent-success" style="
                                                    padding-left: 2vw;
                                                    position: absolute;
                                                    bottom: 0;
                                                    right: 6rem;
                                                ">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" style="margin-left: 18px;margin-right: 20px" fill="currentColor" class="bi bi-check2-circle" viewBox="0 0 16 16">
                                                        <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                                                        <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
                                                    </svg>
                                                    {{ \Session::get('success') }}
                                                </div>
                                            @endif
                                        </div>
                                        <div style="
                                            width: 100%;
                                            height: 90%;
                                        ">
                                            <div style="
                                                width: 100%;
                                                height: 100%;
                                                display: flex;
                                            ">
                                                <div style="
                                                    width: 100%;
                                                    height: 100%;
                                                ">
                                                    <div style="
                                                        width: 100%;
                                                        height: 100%;
                                                        background: #FFFFFF;
                                                        padding: 24px;
                                                        overflow-x: hidden;
                                                        overflow-y: scroll;
                                                    ">
                                                        <div id="list-search">
                                                            <table class="table table-striped">
                                                                <thead class="thead-dark">
                                                                  <tr>
                                                                    <th scope="col">Nom</th>
                                                                    <th scope="col">Prenom</th>
                                                                    <th scope="col">Sexe</th>
                                                                    <th scope="col">Date de naissance</th>
                                                                    <th scope="col">Lieu de naissance</th>
                                                                    <th scope="col">Fonction</th>
                                                                    <th scope="col">Service</th>
                                                                    <th scope="col">Sous Direction</th>
                                                                    <th scope="col">Direction</th>
                                                                    <th scope="col">Telephone</th>
                                                                    <th scope="col">Adresse</th>
                                                                    <th scope="col">Nom d'utilisateur</th>
                                                                    <th scope="col">Administrateur</th>
                                                                  </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach ($agents as $item)
                                                                    <tr data-toggle="modal" data-whatever="{{ $item->id }}" data-target="#modalUpdate" style="cursor: pointer;">
                                                                        <th scope="row"><?php echo $item->nom; ?></th>
                                                                        <td><?php echo $item->prenom; ?></td>
                                                                        <td><?php echo (!$item->sexe) ? "M" : "F"; ?></td>
                                                                        <td><?php echo $item->date_naissance; ?></td>
                                                                        <td><?php echo $item->lieu_naissance; ?></td>
                                                                        <td><?php
                                                                            $searchFonction = Fonction::find($item->id_fonction);
                                                                            echo $searchFonction->nom;
                                                                        ?></td>
                                                                        <td><?php
                                                                            $searchService = Service::find($item->id_service);
                                                                            echo $searchService->libelle;
                                                                        ?></td>
                                                                        <td><?php
                                                                            $searchServiceSousDirection = Service::find($item->id_service);
                                                                            if (intval($searchServiceSousDirection->id_sous_direction) === 0) {
                                                                                echo '<span style="color: gray;">(Aucune sous direction)</span> Secrétariat';
                                                                            }else{
                                                                                $searchSous_direction = Sous_direction::find($searchServiceSousDirection->id_sous_direction);
                                                                                echo $searchSous_direction->libelle;
                                                                            }
                                                                        ?></td>
                                                                        <td><?php
                                                                            $searchServiceSousDirectionDirection = Service::find($item->id_service);
                                                                            if (intval($searchServiceSousDirection->id_sous_direction) === 0) {
                                                                                $searchDirection = Direction::find(1);
                                                                                echo $searchDirection->libelle;
                                                                            }else{
                                                                                $searchSous_directionDirection = Sous_direction::find($searchServiceSousDirection->id_sous_direction);
                                                                                $searchDirection = Direction::find($searchSous_directionDirection->id_direction);
                                                                                echo $searchDirection->libelle;
                                                                            }
                                                                        ?></td>
                                                                        <td><?php echo $item->tel; ?></td>
                                                                        <td><?php echo $item->adresse; ?></td>
                                                                        <td><?php echo $item->username; ?></td>
                                                                        <td><?php 
                                                                            if ($item->admins) {
                                                                              echo 'Oui';
                                                                            } else {
                                                                              echo 'Non';
                                                                            }
                                                                            ?></td>
                                                                    </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- SLIDER RIGHT --}}
                        <x-comp-slider-right />
                        {{-- FIN SLIDER RIGHT --}}
                    </div>
                </div>
            </div>
        </div>

    </section>

    @if (\Session::has('return'))
        <div class="alert alert-success" id="agent-success" style="
            padding-left: 2vw;
            position: absolute;
            bottom: 0;
            right: 6rem;
        ">
            <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" style="margin-left: 18px;margin-right: 20px" fill="currentColor" class="bi bi-check2-circle" viewBox="0 0 16 16">
                <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
            </svg>
            {{ \Session::get('return') }}
        </div>
    @endif

    <section class="modal fade" id="modalUpdate" tabindex="-1" role="dialog" aria-labelledby="ModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form autocomplete="off" class="modal-content" action="{{action('\App\Http\Controllers\AgentController@update')}}" method="POST">
                {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title" id="ModalLongTitle">Modifier les informations d'un employé</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                {{-- FORM UPDATE --}}
                <div id="form-data-preload"></div>
                {{-- END UPDATE --}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn" style="background-color:#EC9628;">Enregitrer les modifications</button>
                </div>
            </form>
        </div>
    </section>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>