<div style="width: 5vw;height: 100vh;background-image: linear-gradient(#317AC1, #497B96);overflow: hidden;">
    <div style="
        width: 100%;
        height: auto;
        display: flex;
        justify-content: center;
        align-items: center;
        margin-top: 1vw;
    ">
        <a href="/menu/profil" data-toggle="tooltip" title="Profil" style="
            width: 4vw; 
            height: 4vw;
            background: white;
            color: gray;
            padding: 1vw;
            display: flex;
            justify-content: center;
            align-items: center;
            border-radius: 100%;
            overflow: hidden;
        ">
            {{-- <img src="{{asset('img/profil.png')}}" alt="Profil" style="
                width: 3vw; 
                height: auto;
            "> --}}
            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
                <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3Zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6Z"/>
            </svg>
        </a>
    </div>
    <a href="/" data-toggle="tooltip" title="Accueil" style="
        width: 100%; 
        height: auto;
        display: flex;
        justify-content: center;
        align-items: center;
        margin-top: 2rem;
    "><i class="bi bi-house-door" style="
        font-size: 1.5rem; 
        color: #FFFFFF;
        <?php
            $route = Route::current()->uri;
            if ($route === '/') {
        ?>
        color: #EC9628;
        <?php
            }
        ?>
    "></i></a>
    <a href="/menu/profil" data-toggle="tooltip" title="Menu" style="
        width: 100%; 
        height: auto;
        display: flex;
        justify-content: center;
        align-items: center;
    "><i class="bi bi-grid" style="
        font-size: 1.5rem; 
        color: #FFFFFF;
        <?php
            $route = Route::current()->uri;
            if ($route === 'menu') {
        ?>
        color: #EC9628;
        <?php
            }
        ?>
    "></i></a>
    <a href="/messageries" data-toggle="tooltip" title="Messageries" style="
        width: 100%; 
        height: auto;
        display: flex;
        justify-content: center;
        align-items: center;
    "><i class="bi bi-envelope" style="
        font-size: 1.5rem; 
        color: #FFFFFF;
        <?php
            $route = Route::current()->uri;
            if ($route === 'messageries') {
        ?>
        color: #EC9628;
        <?php
            }
        ?>
    "></i></a>
</div>