<?php
    use App\Models\Agent;
    use App\Models\Service;
    use App\Models\fonction;

    $service = Service::all()->sortBy('id');

    $fonction = fonction::all()->sortBy('id');

    $agents = Agent::all()->take(5)->sortBy('id');
    $countAgents = count($agents);

    $id_conneted = $_SESSION['id'];

    $agentConnected = Agent::where('id', $id_conneted)->get();

    $nom_connecte = $agentConnected[0]->nom;
    $prenom_connecte = $agentConnected[0]->prenom;
    $tel_connecte = $agentConnected[0]->tel;
    $fonction_connecte = $agentConnected[0]->id_fonction;
    $service_connecte = $agentConnected[0]->id_service;
    $adresse_connecte = $agentConnected[0]->adresse;
    $email_connecte = $agentConnected[0]->email;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Accueil</title>
    <!-- Fonts -->
    <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.2.2/font/bootstrap-icons.css" rel="stylesheet">

    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>

    <!-- Custom asset -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script defer src="{{asset('js/main.js')}}"></script>
</head>
<body class="antialiased">
    <section>

        <div style="
            display: flex;
        ">
            {{-- SLIDER --}}
                <x-comp-slider />
            {{-- FIN SLIDER --}}

            <div style="
                width: 95vw;
                height: 100vh;
                background: rgb(131, 84, 84);
                background-image: url('{{asset('img/fond.jpg')}}');
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-size: cover;
            ">
                {{-- HEADER--}}
                <x-comp-nav-bar />
                {{-- FIN HEADER --}}

                <div style="width: 100%; height: 92vh; display: flex; align-items: center; jsutify-content: center;">
                    <div style="width: 100%; height: 92vh; display: flex;">
                        <div style="
                            width: 95%;
                            height: 100%;
                            background: rgba(255, 255, 255, 0.4);
                        ">
                            <div style="
                                height: 100%;
                                padding-left: 2%;
                                padding-top: 2%;
                            ">
                                <div style="
                                    width: 100%;
                                    height: 8%;
                                    display: flex;
                                    align-items: center;
                                    background: white;
                                    border-bottom: solid 2px #EC9628;
                                ">
                                    <div style="
                                        display: flex;
                                        align-items: center;
                                        padding-left: 1rem;
                                        font-weight: bold;
                                    ">

                                        <a href="/documents-administratifs" style="
                                            margin-left: 1rem;
                                            height: 100%;
                                            display: flex;
                                            align-items: center;
                                            justify-content: center;
                                            border-bottom: solid 3px black;
                                            margin-bottom: 8px;
                                        ">DOCUMENTS ADMINISTRATIFS</a>
                                    </div>
                                </div>
                                <div style="height: 2%;"></div>
                                <div style="
                                    width: 100%;
                                    height: 90%;
                                ">
                                    <div style="
                                        height: 100%;
                                        border-top: solid 1px #EC9628;
                                    ">
                                        <div style="
                                            width: 100%;
                                            height: 10%;
                                            background: white;
                                            padding-left: 1rem;
                                            font-size: 1.1rem;
                                            font-weight: bold;
                                            display: flex;
                                            align-items: center;
                                            border-bottom: solid 3px gray;
                                        ">
                                            Mon dossier personnel&nbsp;<span style="color: #EC9628;"> / DOCUMENTS ADMINISTRATIFS</span>
                                        </div>
                                        <div style="
                                            width: 100%;
                                            height: 90%;
                                            background: rgb(216, 216, 216);
                                        ">
                                            <div style="
                                                height: 100%;
                                                display: flex;
                                            ">
                                                <div class="column-one" style="
                                                    width: 20%;
                                                    height: 100%;
                                                    background: white;
                                                    overflow-y: scroll;
                                                    overflow-x: hidden;
                                                ">
                                                    <div style="width: 100%;height: 100%;">
                                                        <div class="bouton-one" style="
                                                            width: 100%;
                                                            height: 15%;
                                                            display: flex;
                                                            align-items: center;
                                                            padding-left: 0.5rem;
                                                            font-weight: 600;
                                                            cursor: pointer;
                                                            border-bottom: solid 2px rgba(128, 128, 128, 0.5);
                                                        ">
                                                            <div>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="red" class="bi bi-file-pdf-fill" viewBox="0 0 16 16">
                                                                    <path d="M5.523 10.424c.14-.082.293-.162.459-.238a7.878 7.878 0 0 1-.45.606c-.28.337-.498.516-.635.572a.266.266 0 0 1-.035.012.282.282 0 0 1-.026-.044c-.056-.11-.054-.216.04-.36.106-.165.319-.354.647-.548zm2.455-1.647c-.119.025-.237.05-.356.078a21.035 21.035 0 0 0 .5-1.05 11.96 11.96 0 0 0 .51.858c-.217.032-.436.07-.654.114zm2.525.939a3.888 3.888 0 0 1-.435-.41c.228.005.434.022.612.054.317.057.466.147.518.209a.095.095 0 0 1 .026.064.436.436 0 0 1-.06.2.307.307 0 0 1-.094.124.107.107 0 0 1-.069.015c-.09-.003-.258-.066-.498-.256zM8.278 4.97c-.04.244-.108.524-.2.829a4.86 4.86 0 0 1-.089-.346c-.076-.353-.087-.63-.046-.822.038-.177.11-.248.196-.283a.517.517 0 0 1 .145-.04c.013.03.028.092.032.198.005.122-.007.277-.038.465z"/>
                                                                    <path fill-rule="evenodd" d="M4 0h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2zm.165 11.668c.09.18.23.343.438.419.207.075.412.04.58-.03.318-.13.635-.436.926-.786.333-.401.683-.927 1.021-1.51a11.64 11.64 0 0 1 1.997-.406c.3.383.61.713.91.95.28.22.603.403.934.417a.856.856 0 0 0 .51-.138c.155-.101.27-.247.354-.416.09-.181.145-.37.138-.563a.844.844 0 0 0-.2-.518c-.226-.27-.596-.4-.96-.465a5.76 5.76 0 0 0-1.335-.05 10.954 10.954 0 0 1-.98-1.686c.25-.66.437-1.284.52-1.794.036-.218.055-.426.048-.614a1.238 1.238 0 0 0-.127-.538.7.7 0 0 0-.477-.365c-.202-.043-.41 0-.601.077-.377.15-.576.47-.651.823-.073.34-.04.736.046 1.136.088.406.238.848.43 1.295a19.707 19.707 0 0 1-1.062 2.227 7.662 7.662 0 0 0-1.482.645c-.37.22-.699.48-.897.787-.21.326-.275.714-.08 1.103z"/>
                                                                </svg>
                                                                Attestation de travail
                                                            </div>
                                                        </div>

                                                        <div class="bouton-two" style="
                                                            width: 100%;
                                                            height: 15%;
                                                            display: flex;
                                                            align-items: center;
                                                            padding-left: 0.5rem;
                                                            font-weight: 600;
                                                            cursor: pointer;
                                                            border-bottom: solid 2px rgba(128, 128, 128, 0.5);
                                                        ">
                                                            <div>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="red" class="bi bi-file-pdf-fill" viewBox="0 0 16 16">
                                                                    <path d="M5.523 10.424c.14-.082.293-.162.459-.238a7.878 7.878 0 0 1-.45.606c-.28.337-.498.516-.635.572a.266.266 0 0 1-.035.012.282.282 0 0 1-.026-.044c-.056-.11-.054-.216.04-.36.106-.165.319-.354.647-.548zm2.455-1.647c-.119.025-.237.05-.356.078a21.035 21.035 0 0 0 .5-1.05 11.96 11.96 0 0 0 .51.858c-.217.032-.436.07-.654.114zm2.525.939a3.888 3.888 0 0 1-.435-.41c.228.005.434.022.612.054.317.057.466.147.518.209a.095.095 0 0 1 .026.064.436.436 0 0 1-.06.2.307.307 0 0 1-.094.124.107.107 0 0 1-.069.015c-.09-.003-.258-.066-.498-.256zM8.278 4.97c-.04.244-.108.524-.2.829a4.86 4.86 0 0 1-.089-.346c-.076-.353-.087-.63-.046-.822.038-.177.11-.248.196-.283a.517.517 0 0 1 .145-.04c.013.03.028.092.032.198.005.122-.007.277-.038.465z"/>
                                                                    <path fill-rule="evenodd" d="M4 0h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2zm.165 11.668c.09.18.23.343.438.419.207.075.412.04.58-.03.318-.13.635-.436.926-.786.333-.401.683-.927 1.021-1.51a11.64 11.64 0 0 1 1.997-.406c.3.383.61.713.91.95.28.22.603.403.934.417a.856.856 0 0 0 .51-.138c.155-.101.27-.247.354-.416.09-.181.145-.37.138-.563a.844.844 0 0 0-.2-.518c-.226-.27-.596-.4-.96-.465a5.76 5.76 0 0 0-1.335-.05 10.954 10.954 0 0 1-.98-1.686c.25-.66.437-1.284.52-1.794.036-.218.055-.426.048-.614a1.238 1.238 0 0 0-.127-.538.7.7 0 0 0-.477-.365c-.202-.043-.41 0-.601.077-.377.15-.576.47-.651.823-.073.34-.04.736.046 1.136.088.406.238.848.43 1.295a19.707 19.707 0 0 1-1.062 2.227 7.662 7.662 0 0 0-1.482.645c-.37.22-.699.48-.897.787-.21.326-.275.714-.08 1.103z"/>
                                                                </svg>
                                                                Attestation de présence
                                                            </div>
                                                        </div>

                                                        <div class="bouton-three" style="
                                                            width: 100%;
                                                            height: 15%;
                                                            display: flex;
                                                            align-items: center;
                                                            padding-left: 0.5rem;
                                                            font-weight: 600;
                                                            cursor: pointer;
                                                            border-bottom: solid 2px rgba(128, 128, 128, 0.5);
                                                        ">
                                                            <div>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="red" class="bi bi-file-pdf-fill" viewBox="0 0 16 16">
                                                                    <path d="M5.523 10.424c.14-.082.293-.162.459-.238a7.878 7.878 0 0 1-.45.606c-.28.337-.498.516-.635.572a.266.266 0 0 1-.035.012.282.282 0 0 1-.026-.044c-.056-.11-.054-.216.04-.36.106-.165.319-.354.647-.548zm2.455-1.647c-.119.025-.237.05-.356.078a21.035 21.035 0 0 0 .5-1.05 11.96 11.96 0 0 0 .51.858c-.217.032-.436.07-.654.114zm2.525.939a3.888 3.888 0 0 1-.435-.41c.228.005.434.022.612.054.317.057.466.147.518.209a.095.095 0 0 1 .026.064.436.436 0 0 1-.06.2.307.307 0 0 1-.094.124.107.107 0 0 1-.069.015c-.09-.003-.258-.066-.498-.256zM8.278 4.97c-.04.244-.108.524-.2.829a4.86 4.86 0 0 1-.089-.346c-.076-.353-.087-.63-.046-.822.038-.177.11-.248.196-.283a.517.517 0 0 1 .145-.04c.013.03.028.092.032.198.005.122-.007.277-.038.465z"/>
                                                                    <path fill-rule="evenodd" d="M4 0h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2zm.165 11.668c.09.18.23.343.438.419.207.075.412.04.58-.03.318-.13.635-.436.926-.786.333-.401.683-.927 1.021-1.51a11.64 11.64 0 0 1 1.997-.406c.3.383.61.713.91.95.28.22.603.403.934.417a.856.856 0 0 0 .51-.138c.155-.101.27-.247.354-.416.09-.181.145-.37.138-.563a.844.844 0 0 0-.2-.518c-.226-.27-.596-.4-.96-.465a5.76 5.76 0 0 0-1.335-.05 10.954 10.954 0 0 1-.98-1.686c.25-.66.437-1.284.52-1.794.036-.218.055-.426.048-.614a1.238 1.238 0 0 0-.127-.538.7.7 0 0 0-.477-.365c-.202-.043-.41 0-.601.077-.377.15-.576.47-.651.823-.073.34-.04.736.046 1.136.088.406.238.848.43 1.295a19.707 19.707 0 0 1-1.062 2.227 7.662 7.662 0 0 0-1.482.645c-.37.22-.699.48-.897.787-.21.326-.275.714-.08 1.103z"/>
                                                                </svg>
                                                                Attestation d'absence
                                                            </div>
                                                        </div>

                                                        <div class="bouton-for" style="
                                                            width: 100%;
                                                            height: 15%;
                                                            display: flex;
                                                            align-items: center;
                                                            padding-left: 0.5rem;
                                                            font-weight: 600;
                                                            cursor: pointer;
                                                            border-bottom: solid 2px rgba(128, 128, 128, 0.5);
                                                        ">
                                                            <div>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="red" class="bi bi-file-pdf-fill" viewBox="0 0 16 16">
                                                                    <path d="M5.523 10.424c.14-.082.293-.162.459-.238a7.878 7.878 0 0 1-.45.606c-.28.337-.498.516-.635.572a.266.266 0 0 1-.035.012.282.282 0 0 1-.026-.044c-.056-.11-.054-.216.04-.36.106-.165.319-.354.647-.548zm2.455-1.647c-.119.025-.237.05-.356.078a21.035 21.035 0 0 0 .5-1.05 11.96 11.96 0 0 0 .51.858c-.217.032-.436.07-.654.114zm2.525.939a3.888 3.888 0 0 1-.435-.41c.228.005.434.022.612.054.317.057.466.147.518.209a.095.095 0 0 1 .026.064.436.436 0 0 1-.06.2.307.307 0 0 1-.094.124.107.107 0 0 1-.069.015c-.09-.003-.258-.066-.498-.256zM8.278 4.97c-.04.244-.108.524-.2.829a4.86 4.86 0 0 1-.089-.346c-.076-.353-.087-.63-.046-.822.038-.177.11-.248.196-.283a.517.517 0 0 1 .145-.04c.013.03.028.092.032.198.005.122-.007.277-.038.465z"/>
                                                                    <path fill-rule="evenodd" d="M4 0h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2zm.165 11.668c.09.18.23.343.438.419.207.075.412.04.58-.03.318-.13.635-.436.926-.786.333-.401.683-.927 1.021-1.51a11.64 11.64 0 0 1 1.997-.406c.3.383.61.713.91.95.28.22.603.403.934.417a.856.856 0 0 0 .51-.138c.155-.101.27-.247.354-.416.09-.181.145-.37.138-.563a.844.844 0 0 0-.2-.518c-.226-.27-.596-.4-.96-.465a5.76 5.76 0 0 0-1.335-.05 10.954 10.954 0 0 1-.98-1.686c.25-.66.437-1.284.52-1.794.036-.218.055-.426.048-.614a1.238 1.238 0 0 0-.127-.538.7.7 0 0 0-.477-.365c-.202-.043-.41 0-.601.077-.377.15-.576.47-.651.823-.073.34-.04.736.046 1.136.088.406.238.848.43 1.295a19.707 19.707 0 0 1-1.062 2.227 7.662 7.662 0 0 0-1.482.645c-.37.22-.699.48-.897.787-.21.326-.275.714-.08 1.103z"/>
                                                                </svg>
                                                                Attestation de revenu
                                                            </div>
                                                        </div>

                                                        <div class="bouton-five" style="
                                                            width: 100%;
                                                            height: 15%;
                                                            display: flex;
                                                            align-items: center;
                                                            padding-left: 0.5rem;
                                                            font-weight: 600;
                                                            cursor: pointer;
                                                            border-bottom: solid 2px rgba(128, 128, 128, 0.5);
                                                        ">
                                                            <div>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="red" class="bi bi-file-pdf-fill" viewBox="0 0 16 16">
                                                                    <path d="M5.523 10.424c.14-.082.293-.162.459-.238a7.878 7.878 0 0 1-.45.606c-.28.337-.498.516-.635.572a.266.266 0 0 1-.035.012.282.282 0 0 1-.026-.044c-.056-.11-.054-.216.04-.36.106-.165.319-.354.647-.548zm2.455-1.647c-.119.025-.237.05-.356.078a21.035 21.035 0 0 0 .5-1.05 11.96 11.96 0 0 0 .51.858c-.217.032-.436.07-.654.114zm2.525.939a3.888 3.888 0 0 1-.435-.41c.228.005.434.022.612.054.317.057.466.147.518.209a.095.095 0 0 1 .026.064.436.436 0 0 1-.06.2.307.307 0 0 1-.094.124.107.107 0 0 1-.069.015c-.09-.003-.258-.066-.498-.256zM8.278 4.97c-.04.244-.108.524-.2.829a4.86 4.86 0 0 1-.089-.346c-.076-.353-.087-.63-.046-.822.038-.177.11-.248.196-.283a.517.517 0 0 1 .145-.04c.013.03.028.092.032.198.005.122-.007.277-.038.465z"/>
                                                                    <path fill-rule="evenodd" d="M4 0h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2zm.165 11.668c.09.18.23.343.438.419.207.075.412.04.58-.03.318-.13.635-.436.926-.786.333-.401.683-.927 1.021-1.51a11.64 11.64 0 0 1 1.997-.406c.3.383.61.713.91.95.28.22.603.403.934.417a.856.856 0 0 0 .51-.138c.155-.101.27-.247.354-.416.09-.181.145-.37.138-.563a.844.844 0 0 0-.2-.518c-.226-.27-.596-.4-.96-.465a5.76 5.76 0 0 0-1.335-.05 10.954 10.954 0 0 1-.98-1.686c.25-.66.437-1.284.52-1.794.036-.218.055-.426.048-.614a1.238 1.238 0 0 0-.127-.538.7.7 0 0 0-.477-.365c-.202-.043-.41 0-.601.077-.377.15-.576.47-.651.823-.073.34-.04.736.046 1.136.088.406.238.848.43 1.295a19.707 19.707 0 0 1-1.062 2.227 7.662 7.662 0 0 0-1.482.645c-.37.22-.699.48-.897.787-.21.326-.275.714-.08 1.103z"/>
                                                                </svg>
                                                                Attestation d'avancement indiciaire
                                                            </div>
                                                        </div>

                                                        <div class="bouton-six" style="
                                                            width: 100%;
                                                            height: 15%;
                                                            display: flex;
                                                            align-items: center;
                                                            padding-left: 0.5rem;
                                                            font-weight: 600;
                                                            cursor: pointer;
                                                            border-bottom: solid 2px rgba(128, 128, 128, 0.5);
                                                        ">
                                                            <div>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="red" class="bi bi-file-pdf-fill" viewBox="0 0 16 16">
                                                                    <path d="M5.523 10.424c.14-.082.293-.162.459-.238a7.878 7.878 0 0 1-.45.606c-.28.337-.498.516-.635.572a.266.266 0 0 1-.035.012.282.282 0 0 1-.026-.044c-.056-.11-.054-.216.04-.36.106-.165.319-.354.647-.548zm2.455-1.647c-.119.025-.237.05-.356.078a21.035 21.035 0 0 0 .5-1.05 11.96 11.96 0 0 0 .51.858c-.217.032-.436.07-.654.114zm2.525.939a3.888 3.888 0 0 1-.435-.41c.228.005.434.022.612.054.317.057.466.147.518.209a.095.095 0 0 1 .026.064.436.436 0 0 1-.06.2.307.307 0 0 1-.094.124.107.107 0 0 1-.069.015c-.09-.003-.258-.066-.498-.256zM8.278 4.97c-.04.244-.108.524-.2.829a4.86 4.86 0 0 1-.089-.346c-.076-.353-.087-.63-.046-.822.038-.177.11-.248.196-.283a.517.517 0 0 1 .145-.04c.013.03.028.092.032.198.005.122-.007.277-.038.465z"/>
                                                                    <path fill-rule="evenodd" d="M4 0h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2zm.165 11.668c.09.18.23.343.438.419.207.075.412.04.58-.03.318-.13.635-.436.926-.786.333-.401.683-.927 1.021-1.51a11.64 11.64 0 0 1 1.997-.406c.3.383.61.713.91.95.28.22.603.403.934.417a.856.856 0 0 0 .51-.138c.155-.101.27-.247.354-.416.09-.181.145-.37.138-.563a.844.844 0 0 0-.2-.518c-.226-.27-.596-.4-.96-.465a5.76 5.76 0 0 0-1.335-.05 10.954 10.954 0 0 1-.98-1.686c.25-.66.437-1.284.52-1.794.036-.218.055-.426.048-.614a1.238 1.238 0 0 0-.127-.538.7.7 0 0 0-.477-.365c-.202-.043-.41 0-.601.077-.377.15-.576.47-.651.823-.073.34-.04.736.046 1.136.088.406.238.848.43 1.295a19.707 19.707 0 0 1-1.062 2.227 7.662 7.662 0 0 0-1.482.645c-.37.22-.699.48-.897.787-.21.326-.275.714-.08 1.103z"/>
                                                                </svg>
                                                                Attestation de sortir du territoire
                                                            </div>
                                                        </div>

                                                        <div class="bouton-seven" style="
                                                            width: 100%;
                                                            height: 15%;
                                                            display: flex;
                                                            align-items: center;
                                                            padding-left: 0.5rem;
                                                            font-weight: 600;
                                                            cursor: pointer;
                                                            border-bottom: solid 2px rgba(128, 128, 128, 0.5);
                                                        ">
                                                            <div>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="red" class="bi bi-file-pdf-fill" viewBox="0 0 16 16">
                                                                    <path d="M5.523 10.424c.14-.082.293-.162.459-.238a7.878 7.878 0 0 1-.45.606c-.28.337-.498.516-.635.572a.266.266 0 0 1-.035.012.282.282 0 0 1-.026-.044c-.056-.11-.054-.216.04-.36.106-.165.319-.354.647-.548zm2.455-1.647c-.119.025-.237.05-.356.078a21.035 21.035 0 0 0 .5-1.05 11.96 11.96 0 0 0 .51.858c-.217.032-.436.07-.654.114zm2.525.939a3.888 3.888 0 0 1-.435-.41c.228.005.434.022.612.054.317.057.466.147.518.209a.095.095 0 0 1 .026.064.436.436 0 0 1-.06.2.307.307 0 0 1-.094.124.107.107 0 0 1-.069.015c-.09-.003-.258-.066-.498-.256zM8.278 4.97c-.04.244-.108.524-.2.829a4.86 4.86 0 0 1-.089-.346c-.076-.353-.087-.63-.046-.822.038-.177.11-.248.196-.283a.517.517 0 0 1 .145-.04c.013.03.028.092.032.198.005.122-.007.277-.038.465z"/>
                                                                    <path fill-rule="evenodd" d="M4 0h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2zm.165 11.668c.09.18.23.343.438.419.207.075.412.04.58-.03.318-.13.635-.436.926-.786.333-.401.683-.927 1.021-1.51a11.64 11.64 0 0 1 1.997-.406c.3.383.61.713.91.95.28.22.603.403.934.417a.856.856 0 0 0 .51-.138c.155-.101.27-.247.354-.416.09-.181.145-.37.138-.563a.844.844 0 0 0-.2-.518c-.226-.27-.596-.4-.96-.465a5.76 5.76 0 0 0-1.335-.05 10.954 10.954 0 0 1-.98-1.686c.25-.66.437-1.284.52-1.794.036-.218.055-.426.048-.614a1.238 1.238 0 0 0-.127-.538.7.7 0 0 0-.477-.365c-.202-.043-.41 0-.601.077-.377.15-.576.47-.651.823-.073.34-.04.736.046 1.136.088.406.238.848.43 1.295a19.707 19.707 0 0 1-1.062 2.227 7.662 7.662 0 0 0-1.482.645c-.37.22-.699.48-.897.787-.21.326-.275.714-.08 1.103z"/>
                                                                </svg>
                                                                Autorisation spéciale pour absence
                                                            </div>
                                                        </div>

                                                        <div class="bouton-height" style="
                                                            width: 100%;
                                                            height: 15%;
                                                            display: flex;
                                                            align-items: center;
                                                            padding-left: 0.5rem;
                                                            font-weight: 600;
                                                            cursor: pointer;
                                                        ">
                                                            <div>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="red" class="bi bi-file-pdf-fill" viewBox="0 0 16 16">
                                                                    <path d="M5.523 10.424c.14-.082.293-.162.459-.238a7.878 7.878 0 0 1-.45.606c-.28.337-.498.516-.635.572a.266.266 0 0 1-.035.012.282.282 0 0 1-.026-.044c-.056-.11-.054-.216.04-.36.106-.165.319-.354.647-.548zm2.455-1.647c-.119.025-.237.05-.356.078a21.035 21.035 0 0 0 .5-1.05 11.96 11.96 0 0 0 .51.858c-.217.032-.436.07-.654.114zm2.525.939a3.888 3.888 0 0 1-.435-.41c.228.005.434.022.612.054.317.057.466.147.518.209a.095.095 0 0 1 .026.064.436.436 0 0 1-.06.2.307.307 0 0 1-.094.124.107.107 0 0 1-.069.015c-.09-.003-.258-.066-.498-.256zM8.278 4.97c-.04.244-.108.524-.2.829a4.86 4.86 0 0 1-.089-.346c-.076-.353-.087-.63-.046-.822.038-.177.11-.248.196-.283a.517.517 0 0 1 .145-.04c.013.03.028.092.032.198.005.122-.007.277-.038.465z"/>
                                                                    <path fill-rule="evenodd" d="M4 0h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2zm.165 11.668c.09.18.23.343.438.419.207.075.412.04.58-.03.318-.13.635-.436.926-.786.333-.401.683-.927 1.021-1.51a11.64 11.64 0 0 1 1.997-.406c.3.383.61.713.91.95.28.22.603.403.934.417a.856.856 0 0 0 .51-.138c.155-.101.27-.247.354-.416.09-.181.145-.37.138-.563a.844.844 0 0 0-.2-.518c-.226-.27-.596-.4-.96-.465a5.76 5.76 0 0 0-1.335-.05 10.954 10.954 0 0 1-.98-1.686c.25-.66.437-1.284.52-1.794.036-.218.055-.426.048-.614a1.238 1.238 0 0 0-.127-.538.7.7 0 0 0-.477-.365c-.202-.043-.41 0-.601.077-.377.15-.576.47-.651.823-.073.34-.04.736.046 1.136.088.406.238.848.43 1.295a19.707 19.707 0 0 1-1.062 2.227 7.662 7.662 0 0 0-1.482.645c-.37.22-.699.48-.897.787-.21.326-.275.714-.08 1.103z"/>
                                                                </svg>
                                                                Congés
                                                            </div>
                                                        </div>

                                                        <div style="padding: 1rem;"></div>
                                                    </div>
                                                </div>
                                                <div class="column-two" style="
                                                    width: 30%;
                                                    height: 100%;
                                                    background: rgb(216, 216, 216);
                                                    display: none;
                                                ">
                                                    <div style="width: 100%;height: 100%;">
                                                        <div style="
                                                            width: 100%;
                                                            height: 15%;
                                                            display: flex;
                                                            align-items: center;
                                                        ">
                                                            <div style="
                                                                width: 100%;
                                                                height: 100%;
                                                                display: flex;
                                                                align-items: center;
                                                                justify-content: space-between;
                                                            ">
                                                                <div class="title-docs" style="
                                                                    width: 50%;
                                                                    height: 80%;
                                                                    font-size: 1rem;
                                                                    font-weight: 600;
                                                                    padding-left: 0.5rem;
                                                                    padding-right: 0.5rem;
                                                                    display: flex;
                                                                    align-items: center;
                                                                "></div>
                                                                <div style="
                                                                    padding-right: 0.5rem;
                                                                    width: 50%;
                                                                    height: 80%;
                                                                ">
                                                                    <div id="faire-demande" class="btn btn-secondary" style="
                                                                        width: 100%;
                                                                        height: 80%;
                                                                        display: flex;
                                                                        align-items: center;
                                                                        justify-content: center;
                                                                    ">
                                                                        <div>
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-lg" viewBox="0 0 16 16">
                                                                                <path fill-rule="evenodd" d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"/>
                                                                            </svg>
                                                                            Faire une demande
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="
                                                            width: 100%;
                                                            height: 85%;
                                                            overflow-y: scroll;
                                                            overflow-x: hidden;
                                                        ">
                                                            <div style="
                                                                width: 100%;
                                                                background: white;
                                                            ">
                                                                <div style="
                                                                    width: 100%;
                                                                    display: flex;
                                                                    align-items: center;
                                                                    justify-content: center;
                                                                ">Aucune demande</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="column-three" style="
                                                    width: 50%;
                                                    height: 100%;
                                                    background: rgb(216, 216, 216);
                                                    display: none;
                                                ">
                                                    <div style="width: 100%;height: 100%;">
                                                        <form action="#" style="
                                                            width: 100%;
                                                            height: 100%;
                                                            padding-top: 4%;
                                                        ">

                                                            <div style="width: 100%;height: 70%;overflow-y: scroll;overflow-x: hidden;">
                                                                <div style="font-weight: 600;color: gray;text-align: center;">Un message vous sera envoyé aux contacts suivants: </div>

                                                                <div class="form-row" style="width: 80%;display: flex;flex-direction: row;">
                                                                    <div class="col-md-4 mb-3 p-3">
                                                                        <label for="nom">Nom <span style="color: red;">*</span> </label>
                                                                        <input type="name" class="form-control" id="nom" name="nom" placeholder="nom" value="<?php echo $nom_connecte; ?>" required>
                                                                        @if (count($errors) > 0)
                                                                            @foreach ($errors->all() as $error)
                                                                            <?php
                                                                                $error = intval($error);
                                                                                if($error === 1) echo '<div class="feedback text-danger">Entrer un nom</div>';
                                                                            ?>
                                                                            @endforeach
                                                                        @endif
                                                                    </div>
                                                                    <div class="col-md-4 mb-3 p-3">
                                                                        <label for="prenom">Prenom <span style="color: red;">*</span> </label>
                                                                        <input type="name" class="form-control" id="prenom" name="prenom" placeholder="Prenom" value="<?php echo $prenom_connecte; ?>" required>
                                                                        @if (count($errors) > 0)
                                                                        @foreach ($errors->all() as $error)
                                                                        <?php
                                                                            $error = intval($error);
                                                                            if($error === 1) echo '<div class="feedback text-danger">Entrer un prenom</div>';
                                                                        ?>
                                                                        @endforeach
                                                                    @endif
                                                                    </div>
                                                                    <div class="col-md-4 mb-3 p-3">
                                                                        <label for="tel">Telephone <span style="color: red;">*</span> </label>
                                                                        <input type="tel" class="form-control" id="tel" name="tel" placeholder="Telephone" value="<?php echo $tel_connecte; ?>" required>
                                                                        @if (count($errors) > 0)
                                                                        @foreach ($errors->all() as $error)
                                                                        <?php
                                                                            $error = intval($error);
                                                                            if($error === 2) echo '<div class="feedback text-danger">Entrer un numero de téléphone</div>';
                                                                        ?>
                                                                        @endforeach
                                                                    @endif
                                                                    </div>
                                                                </div>

                                                                <div class="form-row" style="width: 80%;display: flex;flex-direction: row;">
                                                                    <div class="col-md-4 mb-3 p-3">
                                                                        <label for="fonction">fonction <span style="color: red;">*</span></label>
                                                                        <div class="form-group">
                                                                            <select class="form-select form-control" id="fonction" name="fonction" aria-label="Default select" required>
                                                                                @foreach ($fonction as $item)
                                                                                    <option value="{{$item->id}}">{{$item->nom}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4 mb-3 p-3">
                                                                        <label for="service">Service <span style="color: red;">*</span></label>
                                                                        <div class="form-group">
                                                                            <select class="form-select form-control" id="service" name="service" aria-label="Default select" required>
                                                                                @foreach ($service as $item)
                                                                                    <option value="{{$item->id}}">{{$item->libelle}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="form-row" style="width: 80%;display: flex;flex-direction: row;">
                                                                    <div class="col-md-4 mb-3 p-3">
                                                                        <label for="adresse">Adresse</label>
                                                                        <input type="text" class="form-control" id="adresse" name="adresse" value="<?php echo $adresse_connecte; ?>" placeholder="Adresse">
                                                                    </div>
                                                                    <div class="col-md-4 mb-3 p-3">
                                                                        <label for="email">Email <span style="color: red;">*</span></label>
                                                                        <input type="text" class="form-control" id="email" name="email" value="<?php echo $email_connecte; ?>" placeholder="Email" required>
                                                                        @if (count($errors) > 0)
                                                                        @foreach ($errors->all() as $error)
                                                                        <?php
                                                                            $error = intval($error);
                                                                            if($error === 6) echo '<div class="feedback text-danger">Entrer un email</div>';
                                                                        ?>
                                                                        @endforeach
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div style="width: 100%;height: 25%;">
                                                                <div class="form-group" style="
                                                                        width: 100%;
                                                                        height: 100%;
                                                                        margin-top: 3%;
                                                                        display: flex;
                                                                        align-items: center;
                                                                        justify-content: center;
                                                                    ">
                                                                        <div style="
                                                                            width: 100%;
                                                                            height: 100%;
                                                                        ">
                                                                            <div style="
                                                                                width: 100%;
                                                                                height: 30%;
                                                                                display: flex;
                                                                                justify-content: space-between;
                                                                            ">
                                                                                <div></div>
                                                                                <div style="padding-top: 1rem;padding-right: 1rem;">
                                                                                    <button class="btn btn-success" id="submit" type="submit">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-earmark-text" viewBox="0 0 16 16">
                                                                                            <path d="M5.5 7a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1h-5zM5 9.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5z"/>
                                                                                            <path d="M9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.5L9.5 0zm0 1v2A1.5 1.5 0 0 0 11 4.5h2V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h5.5z"/>
                                                                                        </svg>

                                                                                        Continuer
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            </div>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- SLIDER RIGHT --}}
                        <x-comp-slider-right />
                        {{-- FIN SLIDER RIGHT --}}
                    </div>
                </div>
            </div>
        </div>

    </section>

    <div class="modal-send" style="display: none;">
        <div style="
            width: 100vw;
            height: 100vh;
            position: absolute;
            top: 0;
            right: 0;
            background: rgba(0,0,0,0.7);
            display: flex;
            align-items: center;
            justify-content: center;
        ">
            <div style="
                width: 50%;
                height: 70%;
                background: white;
                display: flex;
                align-items: center;
                justifty-content: center;
            ">
                <div style="width: 100%;height: 100%;">
                    <div style="font-weight: 600;color: gray;text-align: center;margin-top: 3rem;">information personnel et professionnelle: </div>

                    <div style="display: flex; justify-content: center;padding-top: 5%;">
                        <div style="width: 100%;">
                            <div class="form-group" style="width: 100%;display: flex; align-items: center; justify-content: center;">
                                <div for="email">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" fill="currentColor" class="bi bi-at" viewBox="0 0 16 16">
                                        <path d="M13.106 7.222c0-2.967-2.249-5.032-5.482-5.032-3.35 0-5.646 2.318-5.646 5.702 0 3.493 2.235 5.708 5.762 5.708.862 0 1.689-.123 2.304-.335v-.862c-.43.199-1.354.328-2.29.328-2.926 0-4.813-1.88-4.813-4.798 0-2.844 1.921-4.881 4.594-4.881 2.735 0 4.608 1.688 4.608 4.156 0 1.682-.554 2.769-1.416 2.769-.492 0-.772-.28-.772-.76V5.206H8.923v.834h-.11c-.266-.595-.881-.964-1.6-.964-1.4 0-2.378 1.162-2.378 2.823 0 1.737.957 2.906 2.379 2.906.8 0 1.415-.39 1.709-1.087h.11c.081.67.703 1.148 1.503 1.148 1.572 0 2.57-1.415 2.57-3.643zm-7.177.704c0-1.197.54-1.907 1.456-1.907.93 0 1.524.738 1.524 1.907S8.308 9.84 7.371 9.84c-.895 0-1.442-.725-1.442-1.914z"/>
                                    </svg>
                                </div>
                                <div style="width: 60%;padding-left: 1rem;">
                                    <div style="
                                        width: 100%;
                                        background: rgba(255, 255, 255, 0.329);
                                        padding: 0.5rem;
                                        border-radius: 5px;
                                        border: solid 1px rgba(0, 0, 0, 0.5);
                                    ">
                                        marcmayrand225@gmail.com
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" style="width: 100%;margin-top: 3%;display: flex; align-items: center; justify-content: center;">
                                <div for="email">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" fill="currentColor" class="bi bi-telephone-fill" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"/>
                                    </svg>
                                </div>
                                <div style="width: 60%;padding-left: 1rem;">
                                    <div style="
                                        width: 100%;
                                        background: rgba(255, 255, 255, 0.329);
                                        padding: 0.5rem;
                                        border-radius: 5px;
                                        border: solid 1px rgba(0, 0, 0, 0.5);
                                    ">
                                        0787475899
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" style="
                                width: 100%;
                                margin-top: 3%;
                                display: flex;
                                align-items: center;
                                justify-content: center;
                            ">
                                <div style="
                                    width: 90%;
                                    background: #EC9628;
                                    color: white;
                                    padding: 0.5rem;
                                    border-radius: 3px;
                                    display: flex;
                                    align-items: center;
                                    justify-content: center;
                                ">

                                    <div>
                                        <div>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="red" class="bi bi-file-pdf-fill" viewBox="0 0 16 16">
                                                <path d="M5.523 10.424c.14-.082.293-.162.459-.238a7.878 7.878 0 0 1-.45.606c-.28.337-.498.516-.635.572a.266.266 0 0 1-.035.012.282.282 0 0 1-.026-.044c-.056-.11-.054-.216.04-.36.106-.165.319-.354.647-.548zm2.455-1.647c-.119.025-.237.05-.356.078a21.035 21.035 0 0 0 .5-1.05 11.96 11.96 0 0 0 .51.858c-.217.032-.436.07-.654.114zm2.525.939a3.888 3.888 0 0 1-.435-.41c.228.005.434.022.612.054.317.057.466.147.518.209a.095.095 0 0 1 .026.064.436.436 0 0 1-.06.2.307.307 0 0 1-.094.124.107.107 0 0 1-.069.015c-.09-.003-.258-.066-.498-.256zM8.278 4.97c-.04.244-.108.524-.2.829a4.86 4.86 0 0 1-.089-.346c-.076-.353-.087-.63-.046-.822.038-.177.11-.248.196-.283a.517.517 0 0 1 .145-.04c.013.03.028.092.032.198.005.122-.007.277-.038.465z"/>
                                                <path fill-rule="evenodd" d="M4 0h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2zm.165 11.668c.09.18.23.343.438.419.207.075.412.04.58-.03.318-.13.635-.436.926-.786.333-.401.683-.927 1.021-1.51a11.64 11.64 0 0 1 1.997-.406c.3.383.61.713.91.95.28.22.603.403.934.417a.856.856 0 0 0 .51-.138c.155-.101.27-.247.354-.416.09-.181.145-.37.138-.563a.844.844 0 0 0-.2-.518c-.226-.27-.596-.4-.96-.465a5.76 5.76 0 0 0-1.335-.05 10.954 10.954 0 0 1-.98-1.686c.25-.66.437-1.284.52-1.794.036-.218.055-.426.048-.614a1.238 1.238 0 0 0-.127-.538.7.7 0 0 0-.477-.365c-.202-.043-.41 0-.601.077-.377.15-.576.47-.651.823-.073.34-.04.736.046 1.136.088.406.238.848.43 1.295a19.707 19.707 0 0 1-1.062 2.227 7.662 7.662 0 0 0-1.482.645c-.37.22-.699.48-.897.787-.21.326-.275.714-.08 1.103z"/>
                                            </svg>
                                            Attestation de travail
                                        </div>

                                        <div style="padding-top: 0.5rem;">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="red" class="bi bi-file-pdf-fill" viewBox="0 0 16 16">
                                                <path d="M5.523 10.424c.14-.082.293-.162.459-.238a7.878 7.878 0 0 1-.45.606c-.28.337-.498.516-.635.572a.266.266 0 0 1-.035.012.282.282 0 0 1-.026-.044c-.056-.11-.054-.216.04-.36.106-.165.319-.354.647-.548zm2.455-1.647c-.119.025-.237.05-.356.078a21.035 21.035 0 0 0 .5-1.05 11.96 11.96 0 0 0 .51.858c-.217.032-.436.07-.654.114zm2.525.939a3.888 3.888 0 0 1-.435-.41c.228.005.434.022.612.054.317.057.466.147.518.209a.095.095 0 0 1 .026.064.436.436 0 0 1-.06.2.307.307 0 0 1-.094.124.107.107 0 0 1-.069.015c-.09-.003-.258-.066-.498-.256zM8.278 4.97c-.04.244-.108.524-.2.829a4.86 4.86 0 0 1-.089-.346c-.076-.353-.087-.63-.046-.822.038-.177.11-.248.196-.283a.517.517 0 0 1 .145-.04c.013.03.028.092.032.198.005.122-.007.277-.038.465z"/>
                                                <path fill-rule="evenodd" d="M4 0h8a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2zm.165 11.668c.09.18.23.343.438.419.207.075.412.04.58-.03.318-.13.635-.436.926-.786.333-.401.683-.927 1.021-1.51a11.64 11.64 0 0 1 1.997-.406c.3.383.61.713.91.95.28.22.603.403.934.417a.856.856 0 0 0 .51-.138c.155-.101.27-.247.354-.416.09-.181.145-.37.138-.563a.844.844 0 0 0-.2-.518c-.226-.27-.596-.4-.96-.465a5.76 5.76 0 0 0-1.335-.05 10.954 10.954 0 0 1-.98-1.686c.25-.66.437-1.284.52-1.794.036-.218.055-.426.048-.614a1.238 1.238 0 0 0-.127-.538.7.7 0 0 0-.477-.365c-.202-.043-.41 0-.601.077-.377.15-.576.47-.651.823-.073.34-.04.736.046 1.136.088.406.238.848.43 1.295a19.707 19.707 0 0 1-1.062 2.227 7.662 7.662 0 0 0-1.482.645c-.37.22-.699.48-.897.787-.21.326-.275.714-.08 1.103z"/>
                                            </svg>
                                            Attestation de revenu
                                        </div>
                                    </div>
                                </div>

                                <div style="
                                    width: 50%;
                                    height: 100%;
                                    background: rgba(255, 255, 255, 0.2);
                                    border: solid 1px rgba(0, 0, 0, 0.4);
                                    display: flex;
                                    flex-direction: column;
                                    align-items: center;
                                    margin-top: 1rem;
                                    overflow-y: scroll;
                                    overflow-x: hidden;
                                ">
                                    <div style="
                                        width: 90%;
                                        height: 40%;
                                        border-bottom: solid 2px gray;
                                        display: flex;
                                        align-items: center;
                                        justify-content: center;
                                    ">
                                        <div class="text-success" style="
                                            width: 100%;
                                            display: flex;
                                            align-items: center;
                                            justify-content: space-between;
                                        ">
                                            <div>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-earmark-plus-fill" viewBox="0 0 16 16">
                                                    <path d="M9.293 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.707A1 1 0 0 0 13.707 4L10 .293A1 1 0 0 0 9.293 0zM9.5 3.5v-2l3 3h-2a1 1 0 0 1-1-1zM8.5 7v1.5H10a.5.5 0 0 1 0 1H8.5V11a.5.5 0 0 1-1 0V9.5H6a.5.5 0 0 1 0-1h1.5V7a.5.5 0 0 1 1 0z"/>
                                                </svg>
                                                Mes documents
                                            </div>

                                            <div style="width: 0; height: 0;visibility: hidden;"><input type="file" id="import" name="import" accept="application/pdf" multiple="multiple"></div>

                                            <div>
                                                <label for="import" style="cursor: pointer;">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16">
                                                        <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                                                    </svg>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="previewItem" style="
                                        width: 90%;
                                        height: 100%;
                                    "></div>
                                </div>
                            </div>

                            <div style="display: flex;align-items: center;justify-content: center;margin-top: 1rem;">
                                <button class="btn btn-success" type="submit" onclick="fermerModal()">Valider</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
