<?php 
    use App\Models\Agent;

    $id_conneted = $_SESSION['id'];
    
    $agentsDemande = Agent::where('id', $id_conneted)->get();

    $agents = Agent::all()->sortBy('id');
    $agentlast = $agents->last();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Accueil</title>
    <!-- Fonts -->
    <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.2.2/font/bootstrap-icons.css" rel="stylesheet">
    
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>

    <!-- Custom asset -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script defer src="{{asset('js/main.js')}}"></script>
</head>
<body class="antialiased">
    <section>

        <div style="
            display: flex;
        ">
            {{-- SLIDER --}}
                <x-comp-slider />
            {{-- FIN SLIDER --}}

            <div style="
                width: 95vw;
                height: 100vh;
                background: rgb(131, 84, 84);
                background-image: url('{{asset('img/fond.jpg')}}');
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-size: cover;
            ">
                {{-- HEADER--}}
                <x-comp-header />
                {{-- FIN HEADER --}}

                <div style="width: 100%; height: 92vh; display: flex; align-items: center; jsutify-content: center;">
                    <div style="width: 100%; height: 92vh; display: flex;">
                        <div style="
                            width: 95%;
                            height: 100%;
                            background: rgba(255, 255, 255, 0.4);
                        ">
                            <div style="
                                height: 100%;
                                padding-left: 2%;
                                padding-top: 2%;
                            ">
                                <div style="
                                    width: 100%;
                                    height: 90%;
                                ">
                                    <div style="
                                        height: 100%;
                                        border-top: solid 1px #EC9628;
                                    ">
                                        <div style="
                                            width: 100%;
                                            height: 10%;
                                            background: white;
                                            padding-left: 1rem;
                                            font-size: 1.1rem;
                                            font-weight: bold;
                                            display: flex;
                                            align-items: center;
                                            border-bottom: solid 3px gray;
                                        ">
                                            <span style="color: #EC9628;"> TABLEAU DE BORD</span>
                                        </div>
                                        <div style="
                                            width: 100%;
                                            height: 90%;
                                        ">
                                            <div style="
                                                height: 100%;
                                                display: flex;
                                            ">
                                                <div class="pt-4" style="display: flex;padding-left: 2vw;">
                                                    <div style="
                                                        width: 15vw;
                                                        height: 15vw;
                                                        background: #FFFFFF;
                                                        border-radius: 41px;
                                                        padding: 24px;
                                                    ">
                                                        <div style="font-weight: bold;font-size: 1.245rem">Employé</div>
                                                        <div style="
                                                            display: flex;
                                                            justify-content: space-between;
                                                            align-items: center;
                                                        ">
                                                            <div class="text-lg" style="">
                                                                <div style="
                                                                    font-style: normal;
                                                                    font-weight: bold;
                                                                    color: rgba(0, 0, 0, 0.5);
                                                                ">Total</div>
                                                                <div style="
                                                                    font-style: normal;
                                                                    font-weight: bold;
                                                                ">
                                                                    {{ count($agents) }}
                                                                </div>
                                                            </div>
                                                            <a href="{{ url('liste-agents') }}" style="
                                                                width: 54px;
                                                                height: 54px;
                                                                background: #C0C0C0;
                                                                border-radius: 36px;
                                                                display: flex;
                                                                align-items: center;
                                                                justify-content: center;
                                                            ">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="#454444" class="bi bi-person-lines-fill" viewBox="0 0 16 16">
                                                                    <path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-5 6s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zM11 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5zm.5 2.5a.5.5 0 0 0 0 1h4a.5.5 0 0 0 0-1h-4zm2 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1h-2zm0 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1h-2z"/>
                                                                </svg>
                                                            </a>
                                                        </div>
                                                        <div class="pt-2" style="
                                                            font-style: normal;
                                                            font-weight: bold;
                                                            color: rgba(0, 0, 0, 0.5);
                                                        ">Dernière personne enregistrée</div>
                                                        <div class="pt-2" style="
                                                            font-style: normal;
                                                            font-weight: bold;
                                                            font-size: 1.025rem;
                                                        ">{{ $agentlast->nom }} {{ $agentlast->prenom }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- SLIDER RIGHT --}}
                        <x-comp-slider-right />
                        {{-- FIN SLIDER RIGHT --}}
                    </div>
                </div>
            </div>
        </div>

    </section>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>