<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\PostControllers::class, 'index']);

Route::get('/connexion', [\App\Http\Controllers\PostControllers::class, 'connexion']);
Route::post('/creer-connexion', [\App\Http\Controllers\AgentController::class, 'index']);

Route::get('/logout', [\App\Http\Controllers\PostControllers::class, 'logout']);

Route::get('/messageries', [\App\Http\Controllers\PostControllers::class, 'messageries']);

Route::get('/liste-agents', [\App\Http\Controllers\PostControllers::class, 'agents']);

Route::get('/ajouter-agent', [\App\Http\Controllers\AgentController::class, 'create']);
Route::post('/creer-agent', [\App\Http\Controllers\AgentController::class, 'store']);
Route::post('/menu/modifier-info-personnel', [\App\Http\Controllers\AgentController::class, 'update']);
Route::post('/menu/modifier-info-connexion', [\App\Http\Controllers\AgentController::class, 'update']);

Route::get('/pointages', [\App\Http\Controllers\PostControllers::class, 'pointages']);

Route::get('/menu/documents-administratifs', [\App\Http\Controllers\PostControllers::class, 'documentsAdministratifs']);

Route::get('/menu', [\App\Http\Controllers\PostControllers::class, 'menu']);
Route::get('/menu/profil', [\App\Http\Controllers\PostControllers::class, 'profil']);
Route::get('/menu/modifier-profil', [\App\Http\Controllers\PostControllers::class, 'modifierProfil']);

Route::get('/preload-data-form', [\App\Http\Controllers\PostControllers::class, 'preloadDataForm']);
